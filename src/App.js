/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Pressable,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

const paham = () => alert('oke kamu sudah paham');
const apa = () => alert('apa yang kamu bingungkan');
const App = () => {
  return (
    <View style={styles.container}>
      <View style={[styles.card, styles.elevation]}>
        <View style={styles.imageWrapper}>
          <Image
            style={styles.image}
            source={require('../src/assets/image/binar.png')}
          />
        </View>
        <View>
          <View style={styles.titleWrapper}>
            <Text style={styles.title}>Styling di React Native</Text>
            <Text style={styles.subTitle}>Binar Academy - React Native</Text>
          </View>
          <Text style={styles.text}>
            As a component grows in complexity, it is much cleaner and efficient
            to use StyleSheet.create so as to define severa styles in one place
          </Text>
          <View style={styles.button}>
            <Pressable onPress={() => paham()}>
              <Text style={{color: 'green'}}>Understood!</Text>
            </Pressable>
            <Pressable onPress={() => apa()}>
              <Text style={{color: 'green'}}>What?!!</Text>
            </Pressable>
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  image: {
    height: undefined,
    width: undefined,
    flex: 1,
    resizeMode: 'cover',
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
  },
  imageWrapper: {
    width: 280,
    height: 280,
  },
  card: {
    width: 280,
    borderRadius: 8,
    marginTop: 40,
    backgroundColor: 'white',
  },
  elevation: {
    elevation: 20,
    shadowColor: '#52006A',
  },
  container: {
    alignItems: 'center',
  },
  titleWrapper: {
    alignItems: 'center',
  },
  button: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginVertical: 40,
  },
  text: {
    paddingHorizontal: 16,
    textAlign: 'left',
    fontSize: 12,
  },
  title: {
    marginTop: 10,
    fontWeight: '600',
  },
  subTitle: {
    color: '#AD8B73',
    fontSize: 12,
  },
});

export default App;
